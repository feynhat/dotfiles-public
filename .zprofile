#!/bin/zsh

# zsh profile file. Runs on login. Environmental variables are set here.

# If you don't plan on reverting to bash, you can remove the link in ~/.profile
# to clean up.

#rm ~/.local/share/zsh_last_dir

# Adds `~/.local/bin` to $PATH
export PATH="$(du $HOME/.local/bin | cut -f2 | tr '\n' ':')$PATH"

# Default programs:
export EDITOR="vim"
export TERMINAL="alacritty"
export BROWSER="brave"
export READER="zathura"
export LS_COLORS=$LS_COLORS:'di=0;34:ow=0;34'

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # This line will break some DMs.
export LESSHISTFILE="-"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
#export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"

# set PATH so it includes user's private bin if it exists
if [ -d "/mnt/Data/bin" ] ; then
	PATH="/mnt/Data/bin:$PATH"
fi

export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"

export PATH="$GEM_HOME/bin:$PATH"
export PATH="/mnt/Data/proj/macaulay2/build/bin:$PATH"
export PATH="~/.dotnet/tools:$PATH"

export _JAVA_AWT_WM_NONREPARENTING=1
export AWT_TOOLKIT=MToolkit
export DOTNET_CLI_TELEMETRY_OPTOUT=1
