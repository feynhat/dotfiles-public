set ts=4 sts=4 sw=4 noet ai

set number

set nocompatible
syntax on
filetype plugin indent on
set hlsearch

set listchars=eol:¬,tab:>·,trail:~,extends:>

au BufNewFile *.c 0r ~/.vim/templates/template.c
au BufNewFile *.cpp 0r ~/.vim/templates/template.cpp
au BufNewFile *.cxx 0r ~/.vim/templates/template.cxx

au FileType tex,text,html,htm,markdown setl spell spelllang=en_us
au FileType html,htm,htmldjango,css,tex,xml,rss,markdown setl ts=2 sts=2 sw=2 et
au FileType haskell,hs,py,js setl ts=4 sts=4 sw=4 et

let mapleader = ","

map <C-Z> :set list<CR>
map <C-X> :set nolist<CR>
map <C-H> :nohl<CR>

nnoremap <F5> "=strftime("%F")<CR>P
inoremap <F5> <C-R>=strftime("%F")<CR>

"inoremap <Space><Space> <Esc>/<++><Enter>"_c4l

"autocmd FileType html,htm,htmldjango inoremap ;i <em></em><Space><++><Esc>FeT>i
"autocmd FileType html,htm,htmldjango inoremap ;b <b></b><Space><++><Esc>FeT>i

map ,m a{{<raw>}}\(\){{</raw>}}<ESC>hhhhhhhhhhhi
map ,M a{{<raw>}}\[\]{{</raw>}}<ESC>hhhhhhhhhhhi

call plug#begin('~/.vim/plugged')
	Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
	Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
	Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
	Plug 'vim-airline/vim-airline'
	Plug 'whonore/Coqtail'
	Plug 'petRUShka/vim-sage'
	Plug 'coreysharris/Macaulay2.vim'
call plug#end()

let g:tex_flavor='latex'
let g:livepreview_previewer = 'alacritty -e zathura'

set clipboard=unnamedplus

"set background=dark
"colorscheme ron

augroup my_colorschemes
	au!
	au Colorscheme vim-nightfly-colors hi Normal ctermbg=NONE
	hi MatchParen cterm=bold ctermbg=190 ctermfg=Red
	hi Visual cterm=bold ctermbg=White ctermfg=Black
	hi clear SpellBad
	hi SpellBad cterm=underline ctermbg=187 ctermfg=131
augroup END

"hi clear SpellBad
"hi clear SpellCap
hi clear SpellLocal
"hi clear SpellRare
"hi SpellBad cterm=underline
"hi SpellCap cterm=underline
hi SpellLocal cterm=underline
"hi SpellRare cterm=underline

let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
	set pastetoggle=<Esc>[201~
	set paste
	return ""
endfunction

"nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>
