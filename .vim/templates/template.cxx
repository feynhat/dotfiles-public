#include <bits/stdc++.h>

#define fori(i,b,e) for (int i = (b); i < (e); ++i)
#define pb(x) push_back(x)
#define mp(x,y) make_pair(x,y)
#define cs(s) s.c_str()

using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<bool> vbool;
typedef vector<long long> vll;
typedef vector<vi> vvi;
typedef vector<ii> vii;

inline int getInt() { int x; cin >> x; return x; }
inline ll getLL() { ll x; cin >> x; return x; }
inline string getStr() { string x; cin >> x; return x; }

int main()
{
	std::ios::sync_with_stdio(false);
	#ifdef JUDGE
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	#endif
	return 0;
}

