#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias lf='lfub'
PS1='[\u@\h \W]\$ '

export XDG_CONFIG_HOME="$HOME/.config"
export TERMINAL=alacritty

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
